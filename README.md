# .NET Core 2.1 / ASP .NET Core
**.NET Core** is a development platform that let you build and deploy .NET applications in multiple platforms. **ASP .NET Core** is the framework for developing web applications on previously mentioned .NET Core platform.

### ***.NETStandard***:
It's the definition of the API that .NET Core or .NET Framework must implement for being able to use it. This specification also helps to create and mantain libraries that could be used for both platforms.

## How it works?
in **ASP .NET Core** the request and response lifecycle works as a composition of middlewares that are able to write the response and break the flow in any moment. Actually, API Controllers acts as a middleware that handles the request incoming from the request pipeline and map those request context's route with action that connects with our domain code and return a proper response.

We are able to setup this request pipeline in *Startup.Configure(IApplicationBuilder app)* using the following extensions/methods:

***app.Use:*** This allow us to have access to the request context and the next middleware execution task

***app.Run:*** You could access to the request context and be able to perform some action.

***app.Map:*** You can configure a whole request pipeline for a specific route.

*NOTE:* In all of the above methods you would be able to access the current request and response. Request and Response objects are Read and Write Streams that you have to be careful to work with because if you read it or write it, they will become unavailable for the next request pipeline's middlewares.

# ASP .NET MVC Framework
In legacy ASP .NET Framework Applications we have 2 ways to intercept incoming requests and execute some tasks. 

**HttpModule:** These modules are tighly coupled with IIS. It offer us 22 different events that ocurres since the request started to be processed. It also trigger a default HTTP Handler. This HTTP Handler can also be developed and being mapped with an endpoint. Http Handlers and Modules configuration are available in *web.config* file.

**Filters:** We also have filters. These are more focused on controller's actions. It exists 5 types of filters and these are configured by code itself.

# IIS, Application Pools and How Threading works in Web Applications?

**IIS** is a web server that is only available in windows environments It is the root where we can configure all components used for serving web applications from IIS. *Ex: Logging, Certificates, ApplicationPools, Websites, etc.*

**Application Pools** are one of the most important component for deploying web applications in IIS. Our websites must be attached with an application pool. Many websites can be used from the same Application Pool. It could allow us to share and improve caching between those websites and share the same configuration for all related websites (*less effort*). There are several cons of using the same application pool for many web sites. Some of them could be that you need a different account to be used for a specific web application(website), you want to avoid restarting all websites related with a application pool if someone of them crashes. Application Pools spawns its own system process (w3wp.exe), that's why it is a good practice to configure a different application pool for each website. In other words, isolate websites using their own application pool.

**Threading** ... TODO

# HTTPS and SSL/TLS

It's a protocol implemented on top of *HTTP* using *SSL/TLS* security mechanisms. It uses a public key shared between client and server for encryption purposes. This key is retrieved  in negotiation phase. Clients must trust the CA that the server uses and use the public key created as the result of this process.

This way data is being transfered encrypted and only the server and the client  can decrypt it using their own private keys those are not exposed never.

*CA:* Certificate Authority. It's an entity that issues digital certificates.


#   PostgreSql

It's a very powerful open source relational database engine

### Some advantages:
* More support of data types as JSONB, XML or key-value (Hstore)
* Extensibilty to support store procedures or provide PostGIS functionality
* It has a big and collaborative community
* Cross Platform

### Get Started
 . . .

# Entity Framework and Npgsql
**Npgsql** is a open source ADO .NET Data Provider for PostgreSQL and **Entity Framework** is a Object Relational Mapper (ORM) that allow us to perform database operations in a Object Oriented way.

### Setup
...

# Some OOP Concepts
This paradigm is where we build our software thinking everything as a object.
We could refeer Class as a definition of how the object should work. state and behaviour.
Object would be the instantiation of a Class. It uses Class definition and it has its own state. we can instantiate multiple instances of the same class.
We must apply the following concepts to write OOP kind of software:
### *Abstraction:*
It is about hiding implementation details or how internally object does work.
### *Polyformism:*
It is how our object works (specifically methods) based on input parameters (overloading) or overriding if we apply inheritance before.
### *Encapsulation:*
It is also tied with abstraction but it refeers about hiding data. put it inside an object and restrict getters or setters using the proper access level.
### *Inheritance:*
We could reuse some base classes implementations using inheritance. We can inherit interface declarations or classes that contains the default implementation.

# SOLID Principles
These are some principles that we should follow for building high quality software. Most of them lead to use interface and in a good way.

### *(S)ingle Responsibility:*
One class or even a method must be do only one thing. This way when we modify some functionality it  wont affect other functionality.
### *(O)pen for Extension, Closed for Modification:*
We can achieve this declaring before how we call the method using an interface and then implement its own implementations of that. This way we dont touch other classe implementation and increase the maintanbility.
### *(L)iskov Substitution Principle:*
We should be able to replace on implementation using a new  implementation of a interface or abstract class without breaking before implementation
### *(I)nterface Seggregation:*
We should enforce to implement only needed declaration. We should care of how we compose or interface for a specific class
### *(D)ependency Inversion control:*
We should work on interfaces instead of concrete classes. This way our software becomes plugeable. Easy to make modifications.


# Clean Code
## Why should we care about writing good code?
Writing *messy code* could lead us to deliver code "fast" in short term, but this would slow the development in a long term.

-*"Making messes is always slower than staying clean"*

-*"The only way to go fast, is to go well"*
 
 There are some techniques and best practices that would lead you to write *clean code*.
 
 These are some of them:

 -**Meaningful names:** one of the most common thing in software development would be naming things. They could be variables, classes, interfaces, etc. These names must mean something to all people that read your code.

 -**Self descriptive:** Your code should not need comments to understand it. Applying the previously mentioned *"meaningful names"* would help you to achieve it. *"Don't coment bad code - rewrite it."*

 -**Test Covered:** Your code should have tests or at least must be easy to implement them.

-**Without duplicates:** Your code shouldn't have code duplication. If it so, there is something that you are doing wrong.

-**Consistency:** You should pick one word per concept and follow this coding standard/rule in the whole project.

You are writing code that will be read by other programmers and that must be something that we must keep in mind in the development process. Your code must be easy to mantain by others and yourself.


# Repository Pattern
...
# Specification Pattern
...
# Testing
### Unit Testing
- Mocks and Stubs: ...
### TDD
- Red, Green and Red Cycle ....
### BDD
- TBD
### EndTesting
- TBD

# Concurrency and Parallelism
### Differences between these:
....
### How it works internally (async and await)

# Garbage Collector

# Security
### OWASP
### OAuth, OpenID
### Authorization : Claims and other stuff ...
### JWT






