﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace EFandNpsqlPlayground
{
    #region SpaguettiCode Program Startup

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to EF and Npgsql Playground Project");

            using (var ctx = new FooDbContext("Host=localhost;Database=foo;Username=postgres;Password=root"))
            {
                Console.WriteLine("Initial Database Data: ");
                LogDbContextData();

                while (!Console.KeyAvailable)
                {
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.U:
                            Console.WriteLine("Adding new User");
                            ctx.Users.Add(new User { FirstName = "Robinson", LastName = "Villegas" });
                            break;
                        case ConsoleKey.P:
                            //TODO: Associate with existing user :)
                            Console.WriteLine("Adding new Project");
                            ctx.Projects.Add(new Project { Name = "FooProject" });
                            break;
                        case ConsoleKey.R:
                            Console.WriteLine("Adding new Repository");
                            ctx.Repositories.Add(new Repository { GitUrl = "https://bitbucket.org/rvillegas/FooRepo.git" });
                            break;
                        case ConsoleKey.H:
                        case ConsoleKey.Help:
                            Console.WriteLine("Help");
                            break;
                        case ConsoleKey.S:
                            Console.WriteLine("Saving Changes");
                            ctx.SaveChanges();
                            break;
                        case ConsoleKey.Q:
                            Console.WriteLine("Querying");
                            LogDbContextData();
                            break;
                        default:
                            Console.WriteLine("Unhandled key has been entered");
                            break;
                    }
                }

                void LogDbContextData()
                {
                    Console.WriteLine($"Current Projects Count: {ctx.Projects.Count()}");
                    Console.WriteLine($"Current Users Count: {ctx.Users.Count()}");
                    Console.WriteLine($"Current Repositories Count: {ctx.Repositories.Count()}");
                }
            }
        }
    }

    #endregion

    #region Repository and Specification Pattern

    class UserRepository : BaseRepository<User>
    {
        public UserRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }

    abstract class BaseRepository<TModel> : IRepository<TModel> where TModel: class, IModel
    {
        private readonly DbContext _dbContext;

        protected BaseRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        TModel IRepository<TModel>.Get(int id) 
            => _dbContext.Set<TModel>().FirstOrDefault(model => model.Id == id);

        IEnumerable<TModel> IRepository<TModel>.Get() => throw new NotImplementedException();

        void IRepository<TModel>.Add(TModel model) => throw new NotImplementedException();

        void IRepository<TModel>.Update(TModel model) => throw new NotImplementedException();

        void IRepository<TModel>.Delete(TModel model) => throw new NotImplementedException();
    }

    interface IRepository<TModel>
    {
        TModel Get(int id);

        IEnumerable<TModel> Get();

        void Add(TModel model);

        void Update(TModel model);

        void Delete(TModel model);
    }

    #endregion

    #region DbContext and Models

    class FooDbContext : DbContext
    {
        private readonly string _connectionString;

        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Repository> Repositories { get; set; }

        public FooDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_connectionString);
        }
    }

    class Project : IModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OwnerId { get; set; }
        public User Owner { get; set; }
        public List<Repository> Repositories { get; set; }
    }

    class User : IModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
    }

    class Repository : IModel
    {
        public int Id { get; set; }
        public string GitUrl { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }

    interface IModel
    {
        int Id { get; set; }
    }

    #endregion

    #region EF DbContext Factory Stuff

    // NOTE: This implementation is needed for performing migrations because we are not using asp .net core for this project
    // ReSharper disable once UnusedMember.Global
    class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<FooDbContext>
    {
        public FooDbContext CreateDbContext(string[] args)
        {
            return new FooDbContext("Host=localhost;Database=foo;Username=postgres;Password=root");
        }
    }

    #endregion

}